import requests
from bs4 import BeautifulSoup
import operator

# extracts poem from url

def start(url):
    word_list = []
    source_code = requests.get(url).text
    soup = BeautifulSoup(source_code)
    for post_text in soup.findAll('div', {"class": "poem"}):
        content = post_text.p.text
        words = content.lower().split()
        for each_word in words:
            word_list.append(each_word)
    clean_up_list(word_list)


# removes symbols from poem text

def clean_up_list(word_list):
    clean_word_list = []
    for word in word_list:
        symbols = "!@#$%^&*()_+{}:\"<>?,./;'[]-=„–"
        for i in range(0, len(symbols)):
            word = word.replace(symbols[i], "")
        if len(word) > 0:
            clean_word_list.append(word)
    anagram_dic(clean_word_list)


def anagram_dic(words):
    anagram_dic = {}
    for word in words:
        sorted_word = ''.join(sorted(list(word)))
        if sorted_word not in anagram_dic:
            anagram_dic[sorted_word] = []
        if word not in anagram_dic[sorted_word]:
            anagram_dic[sorted_word].append(word)
    for key, value in sorted(anagram_dic.items(), key=operator.itemgetter(0)):
        if len(value) > 1:
            print(key, value)


start('http://ro.wikisource.org/wiki/Luceaf%C4%83rul_(Eminescu)')